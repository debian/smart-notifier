# Get the upstream version number from the debian changelog ;)
VERSION=$(shell dpkg-parsechangelog | grep '^Version: ' | sed 's/^Version: \(.*\)-[^-]*$$/\1/')
FULLVERSION=$(shell dpkg-parsechangelog | grep '^Version: ' | sed 's/^Version: //')

all:
	# nothing

clean:
	rm -rf buildpkg
	rm -rf build
	rm -rf src/smart_notifier.egg-info
	find -name '*.pyc' -exec rm {} \;

clean-deb:
	fakeroot debian/rules clean

deb: clean-deb
	svn up
	[ `svn st | grep -c ""` == 0 ] # Check if directory is clean
	mkdir buildpkg
	svn export . buildpkg/smart-notifier-$(VERSION)
	echo -n $(VERSION) >  buildpkg/smart-notifier-$(VERSION)/src/smart_notifier/version.txt
	cd buildpkg && tar -czf smart-notifier_$(VERSION).orig.tar.gz \
				smart-notifier-$(VERSION)
	cd buildpkg/smart-notifier-$(VERSION) && sidpdebuild
	cd buildpkg && lintian -i smart-notifier_$(FULLVERSION)_i386.changes
