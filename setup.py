import os
from setuptools import setup, find_packages

def get_version():
    dir = os.path.dirname(__file__)
    fn = os.path.join(dir, 'src', 'smart_notifier', 'version.txt')
    f = open(fn, 'r')
    result = f.read()
    f.close()
    print result
    return result

# Setup SchoolTool
setup(
    name="smart_notifier",
    description="graphical hard disk health status notifier",
    long_description="""
A graphical notifier for smartmontools which attempts to show messages from
the smartd disk monitoring daemon to the user. Currently only for gtk.
.
WARNING: This package is not a replacement for regular backups. It will _try_
to warn the user when smartd sends a disk health warning. But success is not
guaranteed. At best this package is an extra layer of protection.
""",
    version=get_version(),
    url='http://lentejasverdes.ath.cx/svn/smart-notifier/',
    license="GPL",
    maintainer="Brian Sutherland",
    maintainer_email="jinty@web.de",
    platforms=["any"],
    package_dir={'': 'src'},
    package_data={'': ['*.glade', '*.txt']},
    scripts=['scripts/smart-notifier'],
    packages=find_packages('src'),
    zip_safe=False,
    )


