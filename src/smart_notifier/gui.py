#
#This file is part of smart-notifier, a graphical disk health notifier
#
#Copyright (C) 2005, 2006  Brian Sutherland <jinty@web.de>
#
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

import os
import smart_notifier
import gtk # bletch, gtk import can fail if we do not have a GUI
import gtk.glade

here = os.path.dirname(__file__)
# Paths to glade files
warning_window = os.path.join(here, 'smart-notifier.glade')

def warn_user(msg):
    #Make the window
    ui = gtk.glade.XML(warning_window)
    #self.ui.signal_connect('end', gtk.main_quit)
    #Customize message
    textbuffer = gtk.TextBuffer()
    textbuffer.set_text(msg)
    textbox = ui.get_widget('textview1')
    textbox.set_buffer(textbuffer)
    # Fix the icon size XXX -- this is temporary intil gazpacho works
    icon = ui.get_widget('image1')
    icon.set_from_stock(gtk.STOCK_DIALOG_WARNING, gtk.ICON_SIZE_DIALOG)
    # Showit
    window = ui.get_widget('window1')
    window.show_all()

def service():
    smart_notifier.BUS.add_signal_receiver(warn_user,
                                           signal_name='warn_user',
                                           dbus_interface=smart_notifier.UD_INTERFACE,
                                           #named_service=smart_notifier.UD_SERVICE, - thiss fails
                                           path=smart_notifier.UD_SERVICE)
    gtk.main()
